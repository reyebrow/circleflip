This module provides integration with the Tympanus Circle Flip slideshow plugin, http://tympanus.net/codrops/2013/01/28/flipping-circle-slideshow/.

In its current state, slideshows can be rendered via the views display formatter.

Installation
------------

1. Normal Drupal module installation

2. Download the Circle Flip source code from http://tympanus.net/codrops/2013/01/28/flipping-circle-slideshow/ and place in /sites/all/libraries and name the folder "circleflip". jQuery 1.8 or higher
   is recommended, so download the jQuery Update module - https://drupal.org/project/jquery_update as well as jQuery Admin Fix module - https://bitbucket.org/reyebrow/re_jquery_admin_fix

3. Enable circleflip and circleflip_views for using Views and CircleFlip.