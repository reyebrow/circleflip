<?php
/**
 * @file
 * Template for a Orbit slideshow
 */
?>
<div id="<?php print $slideshow_id ?>" class="fc-slideshow">
	<ul class="fc-slides">
<?php 
  foreach($slideshow['slides'] as $id => $slide) {
    print '<li';
    if(count($slide['attributes']) > 0) { print drupal_attributes($slide['attributes']); }
    print '>';
    print $slide['markup'];
    if(!empty($slide['caption'])) { 
      print '<h3 id="caption' . $id . '">' . $slide['caption'] . '</h3>';
  	}
    print '</li>';
  }
?>
</ul>
</div>
