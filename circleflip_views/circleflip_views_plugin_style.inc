<?php

/**
 * @file
 * Extension of the Views Plugin Style for circleflip Slideshow
 * Adapted from the Leaflet views module.
 */
class circleflip_views_plugin_style extends views_plugin_style {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['image'] = array('default' => '');
    $options['caption'] = array('default' => '');
    return $options;
  }

  /**
   * Options form
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Get list of fields in this view & flag available geodata fields
    $handlers = $this->display->handler->get_handlers('field');

    $fields = array();
    $fields_data = array();
    foreach ($handlers as $field_id => $handler) {
      $fields[$field_id] = $handler->ui_name();
      /*if (!empty($handler->field_info['type']) && $handler->field_info['type'] == 'image') {
        $fields_data[$field_id] = $handler->ui_name();
      }*/
    }

    // Check whether we have a geofield we can work with
    if (!count($fields)) {
      $form['error'] = array(
        '#markup' => t('Please add at least one imagefield to the view'),
      );
      return;
    }

    $form['image'] = array(
      '#type' => 'select',
      '#title' => t('Image'),
      '#description' => t('Choose the field which will appear as the image.'),
      '#options' => array_merge(array('' => 'None'), $fields),
      '#default_value' => $this->options['image'],
      '#required' => TRUE,
    );

    $form['caption'] = array(
      '#type' => 'select',
      '#title' => t('Caption'),
      '#description' => t('Choose the field which will appear as a caption.'),
      '#required' => FALSE,
      '#options' => array_merge(array('' => 'None'), $fields),
      '#default_value' => $this->options['caption'],
    );

    /* Settings */
    $form['settings'] = array(
      '#type' => 'fieldset', 
      '#title' => t('Slideshow settings'), 
      '#collapsible' => TRUE, 
      '#collapsed' => FALSE,
    );

      $form['settings']['speed'] = array(
        '#title' => t('Animation Speed'),
        '#description' => t('How fast the animations are.'),
        '#type' => 'textfield',
        '#size' => 3,
        '#default_value' => $this->options['settings']['speed'] ? $this->options['settings']['speed'] : '700',
        '#required' => FALSE,
      );

      /*$provide_lib_options = array(
            'js' => t('jquery.circleflip.js'), 
            'css' => t('foundation.css')
        );
      $form['settings']['provide_lib'] = array(
        '#title' => t('Which resources do you need?'),
        '#description' => t('Does your theme already include foundation classes and JS? If so you might want to skip loading them here.'),
        '#type' => 'checkboxes',
        '#options' => $provide_lib_options,
        '#default_value' => isset($this->options['settings']['provide_lib']) ? array_values($this->options['settings']['provide_lib']) : array_keys($provide_lib_options),
        '#required' => FALSE,
      );*/

  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
  }

  /**
   * Renders view
   */
   function render() {
    //dpm($this);
    if ($this->options['image']) {
      // Render the view so that we can access the rendered results...
      $this->render_fields($this->view->result);

      $slides = array();
      $captions = array();

      foreach ($this->view->result as $id => $result) {

        $slideshow['slides'][$id] = array(
            'markup' => '',
            'attributes' => array(),
          );

        $slide_markup = '';
        $caption_markup = '';

        $slideshow['slides'][$id]['attributes']['class'] = array('circleflip-slide');
        
        if (isset($this->options['image'])){
          $field = $result->{'field_'. $this->options['image']};
          // If an image style was not chosen in the view...
          if(empty($field[0]['rendered']['#image_style'])) {
            $slide_markup = theme('image', array(
              'path' => file_create_url($field[0]['raw']['uri']),
              'width' => $field[0]['raw']['width'],
              'height' => $field[0]['raw']['height'],
              'alt' => $field[0]['raw']['alt'],
              'title' => $field[0]['raw']['title'],
            ));
          } else {
            // If an image style was chosen in the view...
            $slide_markup = theme('image_style', array(
              'style_name' => $field[0]['rendered']['#image_style'],
              'path' => $field[0]['raw']['uri'],
              'width' => $field[0]['raw']['width'],
              'height' => $field[0]['raw']['height'],
              'alt' => $field[0]['raw']['alt'],
              'title' => $field[0]['raw']['title'],
              ));
          }
          
          if($this->options['caption'] != '') { 
            $caption_markup = $this->rendered_fields[$id][$this->options['caption']];
          }
        }


        $slideshow['slides'][$id]['markup'] = $slide_markup;
        if($caption_markup != '' ) {
          $slideshow['slides'][$id]['caption'] = $caption_markup;
        }
      }
    }

    // Get the setting of this type of slideshow...
    $settings = $this->options['settings']; //$type['settings'];

    if(count($slideshow) == 0) { return FALSE; }
    // Render the slideshow...
    return circleflip_render_slideshow($slideshow, $settings);
  }
}