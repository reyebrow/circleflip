<?php

/**
 * @file
 * Tympanus circleflip slideshow views integration.
 */

/**
 * Define circleflip views style.
 *
 * Implements hook_views_plugins().
 */
function circleflip_views_views_plugins() {
  $plugins = array(
    'module' => 'circleflip_views',
    'style' => array(
      'circleflip' => array(
        'title' => t('Circleflip Slideshow'),
        'help' => t('Displays a View as a Tympanus Circleflip Slideshow.'),
        'path' => drupal_get_path('module', 'circleflip_views'),
        'handler' => 'circleflip_views_plugin_style',
        'theme' => 'circleflip-slideshow',
        'uses fields' => TRUE,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );

  return $plugins;
}